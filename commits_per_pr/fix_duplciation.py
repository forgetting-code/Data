import os

import pandas

files = os.listdir('.')
files = [file for file in files if 'pagination' not in file]
files = [file for file in files if file.endswith('.csv')]
for file in files:
    if '0' in file:
        continue

    data = pandas.read_csv(file)
    data = data[data['project_id'] != 1]
    data.drop_duplicates(inplace=True)
    data.to_csv(file, index=False)
